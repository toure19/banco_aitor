package com.simarro.practicas.banco_aitor;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.simarro.practicas.banco_aitor.bd.MiBancoOperacional;
import com.simarro.practicas.banco_aitor.pojo.Cliente;

public class LoginActivity extends AppCompatActivity {
    private int TIEMPO_ESPERA = 2000;
    private EditText txtUsuario;
    private EditText txtPass;
    private ProgressBar cargarLogin;
    private MiBancoOperacional mbo;
    private Cliente cli;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsuario = findViewById(R.id.txtUsuario);
        txtPass = findViewById(R.id.txtPass);
        cargarLogin = findViewById(R.id.cargarLogin);
    }

    void cargaSaludo(View v) {

        mbo = MiBancoOperacional.getInstance(this);
        cli = new Cliente();
        cli.setNif(txtUsuario.getText().toString());
        cli.setClaveSeguridad(txtPass.getText().toString());
        /*cli.setNif("11111111A");
        cli.setClaveSeguridad("1234")*/;
        cli = mbo.login(cli);

        if (cli != null) {
            //cargando();
            Intent intent = new Intent(LoginActivity.this, SaludoActivity.class);
            intent.putExtra("CLIENTE", cli);
            startActivity(intent);
        } else {
            //cargando();
            Toast.makeText(this, "NIF o pass incorrectos", Toast.LENGTH_SHORT).show();
            cargarLogin.setVisibility(View.GONE);
        }
    }

    /*public void cargando() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                cargarLogin.setVisibility(View.VISIBLE);
            }
        }, TIEMPO_ESPERA);
    }*/
}
