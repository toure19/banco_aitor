package com.simarro.practicas.banco_aitor.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.simarro.practicas.banco_aitor.R;
import com.simarro.practicas.banco_aitor.adapter.AdaptadorCuentas;
import com.simarro.practicas.banco_aitor.bd.MiBancoOperacional;
import com.simarro.practicas.banco_aitor.pojo.Cliente;
import com.simarro.practicas.banco_aitor.pojo.Cuenta;

import java.util.ArrayList;

public class CuentasFragment extends Fragment implements AdapterView.OnItemClickListener {

    private ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
    private ListView lstListado;
    private MiBancoOperacional mbo;
    private Cliente cli;
    private CuentasListener listener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_lista_cuentas, container, false);
        mbo = MiBancoOperacional.getInstance(getContext());

        return view;
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);

        cli = (Cliente) getArguments().getSerializable("CLIENTE");
        lstListado = getView().findViewById(R.id.lstListado);
        cuentas = mbo.getCuentas(cli);
        lstListado.setAdapter(new AdaptadorCuentas(this, cuentas));
        lstListado.setOnItemClickListener(this);

    }

    public void setCuentasListener(CuentasListener listener) {
        this.listener = listener;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (listener != null) {
            listener.onCuentaSeleccionada((Cuenta) lstListado.getAdapter().getItem(position));
        }
    }
}