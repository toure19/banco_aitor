package com.simarro.practicas.banco_aitor.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.simarro.practicas.banco_aitor.R;
import com.simarro.practicas.banco_aitor.pojo.Cuenta;

import java.util.ArrayList;


public class AdaptadorCuentas extends ArrayAdapter<Cuenta> {
    FragmentActivity context;
    ArrayList<Cuenta> cuentas;

    public AdaptadorCuentas(Fragment context, ArrayList<Cuenta> cuentas) {
        super(context.getActivity(), R.layout.layout_cuenta_lista, cuentas);
        this.context = context.getActivity();
        this.cuentas = cuentas;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.layout_cuenta_lista, null);

        TextView lblIdCliente = (TextView) item.findViewById(R.id.lblIdCliente);
        lblIdCliente.setText("ID Cliente: "+String.valueOf(cuentas.get(position).getCliente().getId()));

        TextView lblIdCuenta = (TextView) item.findViewById(R.id.lblIdCuenta);
        lblIdCuenta.setText("ID Cuenta: "+String.valueOf(cuentas.get(position).getId()));

        TextView lblBanco = (TextView) item.findViewById(R.id.lblBanco);
        lblBanco.setText("Banco: "+ cuentas.get(position).getBanco());

        TextView lblSucursal = (TextView) item.findViewById(R.id.lblSucursal);
        lblSucursal.setText("Sucusal: " +cuentas.get(position).getSucursal());

        TextView lblDc = (TextView) item.findViewById(R.id.lblDc);
        lblDc.setText("DC: "+cuentas.get(position).getDc());

        TextView lblNumeroCuenta = (TextView) item.findViewById(R.id.lblNumeroCuenta);
        lblNumeroCuenta.setText("Numero Cuenta: "+cuentas.get(position).getNumeroCuenta());

        TextView lblSaldo = (TextView) item.findViewById(R.id.lblSaldo);
        lblSaldo.setText("Saldo Actual: "+String.valueOf(cuentas.get(position).getSaldoActual()));

        return (item);
    }
}
