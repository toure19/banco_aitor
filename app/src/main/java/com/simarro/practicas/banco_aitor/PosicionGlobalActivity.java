package com.simarro.practicas.banco_aitor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.simarro.practicas.banco_aitor.bd.MiBancoOperacional;
import com.simarro.practicas.banco_aitor.fragment.CuentasFragment;
import com.simarro.practicas.banco_aitor.fragment.CuentasListener;
import com.simarro.practicas.banco_aitor.fragment.MovimientosFragment;
import com.simarro.practicas.banco_aitor.pojo.Cliente;
import com.simarro.practicas.banco_aitor.pojo.Cuenta;

public class PosicionGlobalActivity extends AppCompatActivity implements CuentasListener {

    private MiBancoOperacional mbo;
    private Cliente cli;
    private CuentasFragment cuentasFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posicion_global);

        mbo = MiBancoOperacional.getInstance(this);
        cli = (Cliente) (this.getIntent().getSerializableExtra("CLIENTE"));
        Bundle args = new Bundle();
        args.putSerializable("CLIENTE", cli);
        cuentasFragment = (CuentasFragment) getSupportFragmentManager().findFragmentById(R.id.frgPosicionGlobal);
        cuentasFragment.setArguments(args);
        cuentasFragment.setCuentasListener(this);
    }

    @Override
    public void onCuentaSeleccionada(Cuenta cuenta) {
        boolean hayMovimiento = getSupportFragmentManager().findFragmentById(R.id.frgMovimiento) != null;
        if (hayMovimiento) {
            ((MovimientosFragment) getSupportFragmentManager().findFragmentById(R.id.frgMovimiento)).mostrarMovimientos(cuenta);
        } else {
            Intent intent = new Intent(this, MovimientoActivity.class);
            intent.putExtra("CUENTA", cuenta);
            startActivity(intent);
        }
    }
}
