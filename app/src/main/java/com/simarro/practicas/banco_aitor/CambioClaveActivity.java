package com.simarro.practicas.banco_aitor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.simarro.practicas.banco_aitor.bd.MiBancoOperacional;
import com.simarro.practicas.banco_aitor.pojo.Cliente;

public class CambioClaveActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText txtPassAntigua;
    private EditText txtPassNueva;
    private EditText txtPassNuevaConf;
    private Button bAceptarCambioPass;
    private MiBancoOperacional mbo;
    private Cliente cli;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambio_clave);

        txtPassAntigua = findViewById(R.id.txtPassAntigua);
        txtPassNueva = findViewById(R.id.txtPassNueva);
        txtPassNuevaConf = findViewById(R.id.txtPassNuevaConf);
        bAceptarCambioPass = findViewById(R.id.bAceptarCambioPass);
        cli = (Cliente) getIntent().getSerializableExtra("CLIENTE");


        bAceptarCambioPass.setOnClickListener(this);
        mbo = MiBancoOperacional.getInstance(this);

    }


    @Override
    public void onClick(View v) {
        String passAntigua = txtPassAntigua.getText().toString();
        String passNue = txtPassNueva.getText().toString();
        String passNue2 = txtPassNuevaConf.getText().toString();
        String mensaje = "";
        if (passAntigua.equals(cli.getClaveSeguridad())) {
            if (passNue.equals(passNue2)) {
                if (passNue.length() > 0) {
                    cli.setClaveSeguridad(passNue);
                    int p = mbo.changePassword(cli);
                    if (p == 1) {
                        mensaje = "Contraseña cambiada correctamente";
                        Intent intent = new Intent(CambioClaveActivity.this, SaludoActivity.class);
                        intent.putExtra("CLIENTE", cli);
                        startActivity(intent);
                    } else {
                        mensaje = "Error al cambiar la contraseña.";
                    }
                } else {
                    mensaje = "La nueva contraseña no puede estar en blanco";
                }
            } else {
                mensaje = "Las contraseñas nuevas deben coincidir";
            }
        } else {
            mensaje = "La contraseña antigua no es correcta";
        }
        Toast.makeText(this, mensaje, Toast.LENGTH_SHORT).show();
    }
}
