package com.simarro.practicas.banco_aitor.adapter;

import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.simarro.practicas.banco_aitor.R;
import com.simarro.practicas.banco_aitor.pojo.Movimiento;

import java.util.ArrayList;

public class AdaptadorMovimientos extends ArrayAdapter<Movimiento> {
    FragmentActivity context;
    ArrayList<Movimiento> movimientos;

    public AdaptadorMovimientos(Fragment context, ArrayList<Movimiento> movimientos) {
        super(context.getActivity(), R.layout.layout_movimiento_lista, movimientos);
        this.context = context.getActivity();
        this.movimientos = movimientos;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View item = inflater.inflate(R.layout.layout_movimiento_lista, null);

        /*TextView idMovimiento = (TextView) item.findViewById(R.id.idMovimiento);
        idMovimiento.setText("ID Movimiento: " + String.valueOf(movimientos.get(position).getId()));

        TextView tipoMovimiento = (TextView) item.findViewById(R.id.tipoMovimiento);
        tipoMovimiento.setText("Tipo : " + String.valueOf(movimientos.get(position).getTipo()));*/

        TextView fechaOperacionMovimiento = (TextView) item.findViewById(R.id.fechaOperacionMovimiento);
        fechaOperacionMovimiento.setText("Fecha Operacion: " + String.valueOf(movimientos.get(position).getFechaOperacion()));

        TextView descripcionMovimiento = (TextView) item.findViewById(R.id.descripcionMovimiento);
        descripcionMovimiento.setText("Descripción: " + movimientos.get(position).getDescripcion());

        TextView importeMovimiento = (TextView) item.findViewById(R.id.importeMovimiento);
        if (movimientos.get(position).getImporte() > 0) {
            importeMovimiento.setTextColor(Color.GREEN);
        } else {
            importeMovimiento.setTextColor(Color.RED);
        }
        importeMovimiento.setText("Importe: " + String.valueOf(movimientos.get(position).getImporte()));

        /*TextView cuentaOrigenMovimiento = (TextView) item.findViewById(R.id.cuentaOrigenMovimiento);
        cuentaOrigenMovimiento.setText("Cuenta Origen: " + movimientos.get(position).getCuentaOrigen().getNumeroCuenta());

        TextView cuentaDestinoMovimiento = (TextView) item.findViewById(R.id.cuentaDestinoMovimiento);
        cuentaDestinoMovimiento.setText("Cuenta Destino: " + movimientos.get(position).getCuentaDestino().getNumeroCuenta());*/

        return (item);
    }

}
