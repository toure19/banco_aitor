package com.simarro.practicas.banco_aitor.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.simarro.practicas.banco_aitor.R;
import com.simarro.practicas.banco_aitor.adapter.AdaptadorMovimientos;
import com.simarro.practicas.banco_aitor.bd.MiBancoOperacional;
import com.simarro.practicas.banco_aitor.pojo.Cuenta;
import com.simarro.practicas.banco_aitor.pojo.Movimiento;

import java.util.ArrayList;

public class MovimientosFragment extends Fragment implements AdapterView.OnItemClickListener {
    private ArrayList<Movimiento> movimientos = new ArrayList<Movimiento>();
    private ListView lstListado;
    private MiBancoOperacional mbo;
    private Cuenta cuenta;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment_lista_movimientos, container, false);
        mbo = MiBancoOperacional.getInstance(getContext());

        return view;
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
    }

    public void mostrarMovimientos(Cuenta cuenta) {
        lstListado = (ListView) getView().findViewById(R.id.lstListadoMovimientos);
        movimientos = mbo.getMovimientos(cuenta);
        lstListado.setAdapter(new AdaptadorMovimientos(this, movimientos));
        lstListado.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Bundle args = new Bundle();
        args.putSerializable("MOVIMIENTO", (Movimiento) parent.getItemAtPosition(position));
        DialogoMovimientos dialogoMovimientos = new DialogoMovimientos();
        dialogoMovimientos.setArguments(args);
        dialogoMovimientos.show(getFragmentManager(), "Movimientos Cuenta");
    }
}
