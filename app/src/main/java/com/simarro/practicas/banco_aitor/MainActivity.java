package com.simarro.practicas.banco_aitor;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.simarro.practicas.banco_aitor.bd.MiBancoOperacional;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity {
    private int TIEMPO_ESPERA = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cargarPreferences();
        iniciarLogin();

    }

    public void iniciarLogin() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent login = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(login);
            }
        }, TIEMPO_ESPERA);
    }

    public void cargarPreferences() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString("pref_origen_key", prefs.getString("pref_origen_key", ""));
        editor.putString("pref_pais_key", prefs.getString("pref_pais_key", ""));
        editor.putBoolean("pref_video_key", prefs.getBoolean("pref_video_key", true));
        editor.putBoolean("pref_musica_key", prefs.getBoolean("pref_musica_key", true));
        editor.commit();
    }
}
