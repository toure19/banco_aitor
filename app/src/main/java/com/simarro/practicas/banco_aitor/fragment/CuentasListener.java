package com.simarro.practicas.banco_aitor.fragment;

import com.simarro.practicas.banco_aitor.pojo.Cuenta;

public interface CuentasListener {
    void onCuentaSeleccionada(Cuenta cuenta);
}
