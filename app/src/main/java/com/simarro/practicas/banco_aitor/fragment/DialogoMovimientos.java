package com.simarro.practicas.banco_aitor.fragment;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.simarro.practicas.banco_aitor.R;
import com.simarro.practicas.banco_aitor.pojo.Movimiento;

public class DialogoMovimientos extends DialogFragment {
    private Movimiento movimiento;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        movimiento = (Movimiento) getArguments().getSerializable("MOVIMIENTO");
        View v = inflater.inflate(R.layout.fragment_dialogo, container, false);
        View tv = v.findViewById(R.id.txtDialogo);
        ((TextView) tv).setText(movimiento.toString());
        return v;
    }

}
