package com.simarro.practicas.banco_aitor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.simarro.practicas.banco_aitor.bd.MiBD;
import com.simarro.practicas.banco_aitor.bd.MiBancoOperacional;
import com.simarro.practicas.banco_aitor.dao.CuentaDAO;
import com.simarro.practicas.banco_aitor.pojo.Cliente;
import com.simarro.practicas.banco_aitor.pojo.Cuenta;
import com.simarro.practicas.banco_aitor.pojo.Movimiento;

import java.util.ArrayList;
import java.util.Date;

public class TransferenciaActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private GridView grdCuentaOrigen;
    private ArrayAdapter<String> adaptador;
    private String[] monedas = new String[]{"€", "$", "£"};
    private Spinner spinner;
    private RadioButton rbPropia;
    private RadioButton rbAjena;
    private EditText etImporte;
    private CheckBox check;
    private ArrayList<Cuenta> listaCuentas;
    private ArrayList<Cuenta> listaTodasCuentas;
    ArrayList<String> listaCuentaPropia;
    //ArrayList<String> listaCuentaAjena;
    private MiBancoOperacional mbo;
    private CuentaDAO cuentaDAO;
    private Cliente cli;
    private GridView grdCuentaPropia;
    //private GridView grdCuentaAjena;
    private LinearLayout linearCuentaPropia;
    private LinearLayout linearCuentaAjena;
    private Cuenta origen = null;
    private Cuenta destino = null;
    private EditText editAjena;
    private Movimiento movimiento = null;
    private Button clicOk;
    private Button clicCancelar;
    private MiBD miBD;
    private Cuenta cuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transferencia);

        rbPropia = findViewById(R.id.cuentaPropia);
        rbAjena = findViewById(R.id.cuentaAjena);
        etImporte = findViewById(R.id.transferenciaImporte);
        check = findViewById(R.id.transferenciaCheck);
        linearCuentaPropia = findViewById(R.id.linearCuentaPropia);
        linearCuentaAjena = findViewById(R.id.linearCuentaAjena);
        grdCuentaPropia = findViewById(R.id.grdCuentaPropia);
        editAjena = findViewById(R.id.editAjena);

        mbo = MiBancoOperacional.getInstance(this);
        cli = (Cliente) (this.getIntent().getSerializableExtra("CLIENTE"));
        cuentaDAO = new CuentaDAO();
        miBD = MiBD.getInstance(this);

        inflarCuentaOrigen();
        inflarCuentaPropia();
        inflarMonedas();
        mostrarOcultarCuentas();
    }

    public void inflarCuentaOrigen() {
        listaCuentas = cuentaDAO.getCuentas(cli);
        listaCuentaPropia = new ArrayList<>();
        for (int i = 0; i < listaCuentas.size(); i++) {
            listaCuentaPropia.add(listaCuentas.get(i).getNumeroCuenta());
        }
        grdCuentaOrigen = findViewById(R.id.grdCuentaOrigen);
        adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaCuentaPropia);
        grdCuentaOrigen.setAdapter(adaptador);
        grdCuentaOrigen.setOnItemClickListener(this);
    }

    public void inflarCuentaPropia() {
        grdCuentaPropia = findViewById(R.id.grdCuentaPropia);
        adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listaCuentaPropia);
        grdCuentaPropia.setAdapter(adaptador);
        grdCuentaPropia.setOnItemClickListener(this);
    }

    public void inflarMonedas() {
        spinner = findViewById(R.id.transferenciaSpinner);
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, monedas);
        adaptador.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item
        );
        spinner.setAdapter(adaptador);
    }

    public void mostrarOcultarCuentas() {
        rbPropia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearCuentaAjena.setVisibility(View.GONE);
                linearCuentaPropia.setVisibility(View.VISIBLE);
            }
        });
        rbAjena.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearCuentaPropia.setVisibility(View.GONE);
                linearCuentaAjena.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        GridView grid = (GridView) parent;
        switch (grid.getId()) {
            case R.id.grdCuentaOrigen:
                origen = listaCuentas.get(position);
                break;
            case R.id.grdCuentaPropia:
                destino = listaCuentas.get(position);
                break;
            default:
                break;
        }
    }

    public void clickOk(View v) {
        Date date = new Date();
        if (rbAjena.isChecked()) {
            if (editAjena.getText().toString().length() > 0) {
                cuenta = new Cuenta();
                cuenta.setNumeroCuenta(editAjena.getText().toString());
                destino = cuenta;
                movimiento = new Movimiento(1111111, 0, date, "transferencia", Float.parseFloat(etImporte.getText().toString()), origen, destino);
                transferencia(movimiento);
            } else {
                Toast.makeText(this, "La cuenta destino no puede estar vacia.", Toast.LENGTH_SHORT).show();
            }
        } else {
            if (destino != null) {
                movimiento = new Movimiento(1111111, 0, date, "transferencia", Float.parseFloat(etImporte.getText().toString()), origen, destino);
                transferencia(movimiento);
            } else {
                Toast.makeText(this, "Debes seleccionar una cuenta destino.", Toast.LENGTH_SHORT).show();
            }
        }
        switch (transferencia(movimiento)) {
            case 0:
                finish(); //cerramos activity y volvemos a menu
                break;
            case 1:
                Toast.makeText(this, "La cuenta destino no existe.", Toast.LENGTH_SHORT).show();
                break;
            case 2:
                Toast.makeText(this, "Saldo insuficiente en la cuenta origen.", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    public int transferencia(Movimiento movimientoTransferencia) {
        if (miBD.existeCuenta(movimientoTransferencia.getCuentaOrigen().getBanco(), movimientoTransferencia.getCuentaOrigen().getSucursal(), movimientoTransferencia.getCuentaOrigen().getDc(), movimientoTransferencia.getCuentaDestino().getNumeroCuenta())) {
            movimientoTransferencia.getCuentaDestino().setBanco(movimientoTransferencia.getCuentaOrigen().getBanco());
            movimientoTransferencia.getCuentaDestino().setSucursal(movimientoTransferencia.getCuentaOrigen().getSucursal());
            movimientoTransferencia.getCuentaDestino().setDc(movimientoTransferencia.getCuentaOrigen().getDc());
            movimientoTransferencia.setCuentaDestino((Cuenta) cuentaDAO.search(movimientoTransferencia.getCuentaDestino())); //Recuperamos datos de la cuenta destino
            if ((movimientoTransferencia.getCuentaOrigen().getSaldoActual() - movimientoTransferencia.getImporte()) >= 0) {
                movimientoTransferencia.getCuentaOrigen().setSaldoActual(movimientoTransferencia.getCuentaOrigen().getSaldoActual() - movimientoTransferencia.getImporte()); //descuento origen
                miBD.actualizarSaldo(movimientoTransferencia.getCuentaOrigen());
                movimientoTransferencia.getCuentaDestino().setSaldoActual(movimientoTransferencia.getCuentaDestino().getSaldoActual() + movimientoTransferencia.getImporte()); //suma destino
                miBD.actualizarSaldo(movimientoTransferencia.getCuentaDestino());
                miBD.insercionMovimiento(movimientoTransferencia); //insercción movimiento en bbdd
            } else {//saldo insuficiente
                return 2;
            }
        } else {//no existe la cuenta destino
            return 1;
        }
        return 0; //si transferencia OK
    }

    public void clickCancelar(View v) {
        finish();
    }

}

