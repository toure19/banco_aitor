package com.simarro.practicas.banco_aitor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.simarro.practicas.banco_aitor.fragment.CuentasFragment;
import com.simarro.practicas.banco_aitor.pojo.Cliente;

public class SaludoActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageButton bPosGlogal;
    private TextView txtSaludo;
    private ImageButton bCambioPass;
    private ImageButton bSalir;
    private ImageButton bTrasnfer;
    private ImageButton bCajeros;
    private Cliente cli;
    private CuentasFragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saludo);

        //Obtenemos la referencia
        txtSaludo = findViewById(R.id.txtSaludo);

        cli = (Cliente) (this.getIntent().getSerializableExtra("CLIENTE"));
        txtSaludo.setText("Bienvenido " + cli.getNombre());

        bPosGlogal = (ImageButton) findViewById(R.id.bPosGlogal);
        bCambioPass = (ImageButton) findViewById(R.id.bCambioPass);
        bSalir = (ImageButton) findViewById(R.id.bSalir);
        bTrasnfer = (ImageButton) findViewById(R.id.bTransfer);
        bCajeros = (ImageButton) findViewById(R.id.bCajeros);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        bPosGlogal.setOnClickListener(this);
        bCambioPass.setOnClickListener(this);
        bSalir.setOnClickListener(this);
        bTrasnfer.setOnClickListener(this);
        bCajeros.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        ImageButton ib = (ImageButton) view;
        Intent intent;
        switch (ib.getId()) {
            case R.id.bPosGlogal:
                intent = new Intent(SaludoActivity.this, PosicionGlobalActivity.class);
                intent.putExtra("CLIENTE", cli);
                startActivity(intent);
                break;
            case R.id.bCambioPass:
                intent = new Intent(SaludoActivity.this, CambioClaveActivity.class);
                intent.putExtra("CLIENTE", cli);
                startActivity(intent);
                break;
            case R.id.bSalir:
                intent = new Intent(SaludoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.bTransfer:
                intent = new Intent(SaludoActivity.this, TransferenciaActivity.class);
                intent.putExtra("CLIENTE", cli);
                startActivity(intent);
                break;
            case R.id.bCajeros:
                if(esAdmin()){
                    intent = new Intent(SaludoActivity.this, CajerosActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(this, "Solo los administradores pueden acceder a este apartado", Toast.LENGTH_LONG).show();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_posGlobal:
                intent = new Intent(SaludoActivity.this, PosicionGlobalActivity.class);
                intent.putExtra("CLIENTE", cli);
                startActivity(intent);
                break;
            case R.id.action_transfer:
                intent = new Intent(SaludoActivity.this, TransferenciaActivity.class);
                startActivity(intent);
                break;
            case R.id.action_cajeros:
                if(esAdmin()){
                    intent = new Intent(SaludoActivity.this, CajerosActivity.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(this, "Solo los administradores pueden acceder a este apartado", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.action_cambioPass:
                intent = new Intent(SaludoActivity.this, CambioClaveActivity.class);
                intent.putExtra("CLIENTE", cli);
                startActivity(intent);
                break;
            case R.id.action_configuracion:
                intent = new Intent(SaludoActivity.this, PreferenciasActivity.class);
                startActivity(intent);
                break;
            case R.id.action_Salir:
                intent = new Intent(SaludoActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                break;
        }
        return true;
    }

    //Retorna true si es usuario con DNI 11111111A
    public boolean esAdmin() {
        return cli.getNif().equals("11111111A");
    }
}
