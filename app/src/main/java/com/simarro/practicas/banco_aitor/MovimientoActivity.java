package com.simarro.practicas.banco_aitor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.simarro.practicas.banco_aitor.fragment.MovimientosFragment;
import com.simarro.practicas.banco_aitor.pojo.Cuenta;

public class MovimientoActivity extends AppCompatActivity {

    private MovimientosFragment movimientosFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movimiento);

        movimientosFragment = (MovimientosFragment) getSupportFragmentManager().findFragmentById(R.id.frgMovimiento);
        movimientosFragment.mostrarMovimientos((Cuenta) getIntent().getSerializableExtra("CUENTA"));
    }
}
